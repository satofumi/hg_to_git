# -*- coding: utf-8 -*-

import sys
import os
import subprocess


def main():
    # 変換元フォルダ指定がなければ戻る。
    if len(sys.argv) <= 1:
        print('usage:\n\t{} <hg repository directory>'.format(__file__))
        return
    from_dir = sys.argv[1]

    # .hg フォルダがなければ戻る。
    if not os.path.isdir(from_dir):
        print('no such directory: {}'.format(from_dir))
        return

    hg_dir = os.path.join(from_dir, '.hg')
    if not os.path.isdir(hg_dir):
        print('no .hg directory: {}'.format(hg_dir))
        return

    # .git フォルダがあればスキップする。
    dest_dir = os.path.basename(os.path.dirname(from_dir))
    git = os.path.join(dest_dir, '.git')
    if not os.path.isdir(git):
        # fast-export で変換を行う。
        os.mkdir(dest_dir)
        cmd = 'cd {} && git init && git config core.ignoreCase false'.format(dest_dir)
        subprocess.call(cmd, shell=True)

        cmd = 'cd {} && e:/mingw/msys/1.0/bin/sh.exe ../fast-export/hg-fast-export.sh -r ../{} --force -fe shift_jis'.format(dest_dir, from_dir)
        subprocess.call(cmd, shell=True)
        subprocess.call('cd {} && git checkout HEAD'.format(dest_dir), shell=True)

    # .gitignore がなければ作成する。
    gitignore = os.path.join(dest_dir, '.gitignore')
    hgignore = os.path.join(from_dir, '.hgignore')
    if not os.path.isfile(gitignore) and os.path.isfile(hgignore):
        with open (gitignore, 'w') as fout:
            with open (hgignore) as f:
                for line in f.readlines():
                    if line[:12] == 'syntax: glob':
                        line = '#' + line
                    fout.write(line)

    # .hgsub にあるプロジェクトを submodule として登録する。
    hgsub = os.path.join(from_dir, '.hgsub')
    if os.path.isfile(hgsub):
        print('git submodule add <git@host.org:user_name/project.git> <name>')
        with open(hgsub) as f:
            for line in f.readlines():
                print(line.rstrip())


if __name__ == '__main__':
    main()
