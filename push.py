# -*- coding: utf-8 -*-
import sys
import os
import subprocess


def main():
    # リポジトリの指定がなければ戻る。
    if len(sys.argv) <= 2:
        print('usage:\n\t{} <user_name> <repository directory>'.format(__file__))
        return
    user_name = sys.argv[1]
    dest_dir = sys.argv[2].strip('/')
    print(dest_dir)

    cmd = 'cd {} && git remote add origin git@bitbucket.org:{}/{}.git'.format(dest_dir, user_name, dest_dir)
    print(cmd)
    subprocess.call(cmd, shell=True)

    cmd = 'cd {} && git push -u origin master'.format(dest_dir)
    subprocess.call(cmd, shell=True)


if __name__ == '__main__':
    main()
