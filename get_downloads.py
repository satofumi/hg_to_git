# -*- coding: utf-8 -*-
import sys
import os
import requests
import re
import subprocess


def main():
    # ダウンロードサイトの指定がなければ戻る。
    if len(sys.argv) <= 1:
        print('usage:\n\t{} <user_name/project_name>'.format(__file__))
        return
    project_name = sys.argv[1]

    os.mkdir('_downloads')

    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
    url = 'https://bitbucket.org/' + project_name + '/downloads'
    page = requests.get(url, headers=headers)

    tbody = re.findall('<tbody>.+?</tbody>', page.content, re.MULTILINE | re.DOTALL)
    links = set()
    for table_text in tbody:
        result = re.findall('href="(.+?)"', table_text)
        for link in result:
            if project_name + '/downloads' in link:
                links.add('https://bitbucket.org' + link)

    for link in links:
        cmd = ['cd', '_downloads', '&&', 'c://WINDOWS/system32/curl.exe', '-LO', link]
        subprocess.call(cmd, shell=True)


if __name__ == '__main__':
    main()
